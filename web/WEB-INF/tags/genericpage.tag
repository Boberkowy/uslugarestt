<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>

<div id="pageheader">
    <jsp:invoke fragment="header"/>

    <div id="nagłówek">
        <h1 id="tytul"><strong><em>RedJava</em></strong></h1>
    </div>

    <div id="menu">
        <ul>
            <li><a href="Main.jsp">Strona Główna</a></li>
            <li><a href="profile.jsp">Moje Konto</a></li>
            <li><a href="szukaj.jsp">Szukaj </a></li>
            <li><a href="">Wyloguj się</a></li>
        </ul>
    </div>

</div>

<div id="body">
    <jsp:doBody/>

</div>
<div id="pagefooter">
    <jsp:invoke fragment="footer"/>
</div>
</body>
</html>