<%--
  Created by IntelliJ IDEA.
  User: bober
  Date: 2016-01-09
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
  <link rel="Stylesheet" type="text/css" href="CSS/login.css" />

  <title>Login </title>
</head>
<body>

<div id="login">

  <h2><span class="fontawesome-lock"></span>Sign In</h2>

  <form action="Main.jsp" method="POST">

    <fieldset>

      <p><label for="email">E-mail address</label></p>
      <p><input type="email" id="email" value="mail@address.com" onBlur="if(this.value=='')this.value='mail@address.com'" onFocus="if(this.value=='mail@address.com')this.value=''"></p> <!-- JS because of IE support; better: placeholder="mail@address.com" -->

      <p><label for="password">Password</label></p>
      <p><input type="password" id="password" value="password" onBlur="if(this.value=='')this.value='password'" onFocus="if(this.value=='password')this.value=''"></p> <!-- JS because of IE support; better: placeholder="password" -->



      <p><input type="submit" value="Sign In"></p>

      Location
    </fieldset>
  </form>
  <br><br>
  <form action="register.jsp">
    <p><input type="submit" value="Register"><a href="register.jsp"></a></p>
  </form>

</div> <!-- end login -->
</body>
</html>






