package model;

/**
 * Created by Boberkowy on 19.01.2016.
 */
public class Person {


    private int id;
    private String username;
    private int age;
    private String eyeColor;
    private int height;
    private String hair;
    private String address;
    private String education;
    private String description;

    public Person(){

    }


    public Person(String username, int age, String eyeColor, int height, String address, String education, String description){
        this.username = username;
        this.age= age;
        this.eyeColor= eyeColor;
        this.height = height;
        this.address= address;
        this.education=education;
        this.description=description;
    }

    public Person(int id, String username, int age, String eyeColor, int height, String address, String education, String description){
        this.username = username;
        this.age= age;
        this.eyeColor= eyeColor;
        this.height = height;
        this.address= address;
        this.education=education;
        this.description=description;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getHair() {
        return hair;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



}
