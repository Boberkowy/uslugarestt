package rest.service;

import dao.RepositoryCatalog;
import dao.iRepositoryCatalog;
import model.Person;
import rest.dto.PersonSummaryDto;
import rest.dto.personDto;
import sun.java2d.cmm.Profile;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Boberkowy on 19.01.2016.
 */
@Path("person")
public class Service {

    iRepositoryCatalog repository = new RepositoryCatalog();
    int currentId = 0;

    @PUT
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(PersonSummaryDto dto){

        Person person = new Person();
        person.setId(currentId);
        person.setAddress(dto.getAddress());
        person.setAge(dto.getAge());
        person.setDescription(dto.getDescription());
        person.setEducation(dto.getEducation());
        person.setEyeColor(dto.getEyeColor());
        person.setHair(dto.getHair());
        person.setUsername(dto.getUsername());
        person.setHeight(dto.getHeight());
        repository.profiles().add(person);

        currentId ++;
        return Response.status(201).build();
    }

    @GET
    @Path("/{personId}")
    @Produces("application/json")
    public Profile get(@PathParam("personId") int id){
        Person person = repository.profiles().get(id);
        if (person == null) {
            return new Profile(long);
        }
        return person;

    @DELETE
    @Path("/")
    @Produces("application/json")
    public Response delete(@PathParam("personId") int id, PersonSummaryDto dto){
        Person person = new Person();
        person.setId(id);
        person.setAddress(dto.getAddress());
        person.setAge(dto.getAge());
        person.setDescription(dto.getDescription());
        person.setEducation(dto.getEducation());
        person.setEyeColor(dto.getEyeColor());
        person.setHair(dto.getHair());
        person.setUsername(dto.getUsername());
        person.setHeight(dto.getHeight());
        repository.profiles().delete(person);
        return Response.status(200).build();
    }

    @GET
    @Path("/all")
    @Produces("application/json")
    public List<Person> getAll(){
        return repository.profiles().getAll();
    }
}
