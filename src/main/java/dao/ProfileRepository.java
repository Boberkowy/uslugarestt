package dao;

import model.Person;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Boberkowy on 19.01.2016.
 */
public class ProfileRepository implements iRepository<Person> {

    private static List<Person> profiles = new ArrayList<Person>();
    private int currentId=1;

    @Override
    public void add(Person person) {
        person.setId(currentId);
        profiles.add(person);
        currentId++;
    }

    @Override
    public void delete(Person person) {
        profiles.remove(person);

    }

    @Override
    public Person get(int id) {
        for (Person current : profiles) {
            if (current.getId() == id)
                return current;
        }
        return null;
    }
    @Override
    public List<Person> getAll() {
        return profiles;
    }
}
