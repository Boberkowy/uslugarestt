package dao;

import model.Person;

/**
 * Created by Boberkowy on 19.01.2016.
 */
public class RepositoryCatalog implements iRepositoryCatalog {
    @Override
    public iRepository<Person> profiles() {
        return new ProfileRepository();
    }
}
