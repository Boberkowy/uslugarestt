package dao;

import model.Person;

/**
 * Created by Boberkowy on 19.01.2016.
 */
public interface iRepositoryCatalog {

    public iRepository<Person> profiles();

}
