package dao;

import java.util.List;

/**
 * Created by Boberkowy on 19.01.2016.
 */
public interface iRepository<TEntity> {
    public void add(TEntity entity);
    public void delete(TEntity entity);
    public TEntity get(int id);
    public List<TEntity> getAll();

}
